# camara respiracion suelo

Proyecto para desarrollar la elaboración de una cámara de respiración de suelo capaz de medir el flujo de CO2, que sea portátil y de bajo costo.  
El desarrollo será realizado en base al prototipo abierto de la comunidad Hackteria documentado en  https://www.hackteria.org/wiki/CO2_Soil_Respiration_Chamber.   

Este proyecto será desarrollado en el marco de la residencia ReGOSH financiadas por CYTED que tendrán lugar en el CTA de Porto Alegre  
Aquí se puede consultar la propuesta https://docs.google.com/document/d/1ah_eeHkKYPDBCph9JqW0EOlclMm_Bu3f2et6f_Qc1qU/edit

### Fundamentación_Concentración de CO2 como indicador de actividad microbiológica del suelo.
La descomposición de la materia orgánica es un proceso ecosistémico mediado por organismos heterótrofos que utilizan al material orgánico muerto 
o detritus como hábitat y fuente de carbono y energía, este gas se produce, fundamentalmente, a través del metabolismo de la microflora y de las 
raíces de la planta, siendo la descomposición microbiana de compuestos orgánicos el proceso más importante que lo genera. Durante la descomposición
una parte del carbono es devuelto a la atmósfera en forma de CO2 , mientras que otra se transforma en otros compuestos más sencillos o se almacena 
en las propias estructuras microbianas (Pérez et al., 1998). En particular, la respiración metabólica de la comunidad de organismos asociados al 
detritus orgánico es el proceso que libera el carbono hacia la atmósfera en forma de CO2 . De esta manera,la respiración heterotrófica contribuye a 
la descomposición, junto a otros procesos como la humificación y la fragmentación del detritus (Carmona et al., 2006). 
Los microorganismos respiran continuamente y la tasa de respiración es un índice confiable de la tasa de crecimiento. Los factores que afectan el 
crecimiento también influyen en la respiración en el mismo grado.
La evolución del CO2 es un parámetro ligado al manejo de materiales orgánicos el cual representa una medición integral de la respiración del suelo,
conocida como respiración edáfica basal (respiración de las raíces, fauna del suelo y la mineralización del carbono a partir de diferentes “pools” 
del carbono de suelo y desechos), es decir, representa la estimación de la actividad microbiana (García y Rivero, 2008). Frankenberger y Dick (1983, citado por Ajwa et al., 1999),
señalaron que existe una relación muy estrecha entre la actividad biológica de un suelo y su fertilidad por lo que parámetros vinculados a la primera
han sido propuestos como indicadores apropiados del mencionado impacto, uno de ellos es la producción de CO2 (como reflejo del sustrato carbonado
consumido por los microorganismos), el carbono o el nitrógeno unido a la biomasa microbiana y la actividad de las enzimas del suelo (Ajwa et al., 1999).


## Tareas

### Diseço de placa
Acá se puede ver la placa en pcb de la cámara de respiración, como algunos complementos que fueron creados para el mismo 
https://gitlab.com/nodo-mendoza/camara-respiracion-suelo/tree/master/hardware/electronics
También pueden ver algunos inconvenientes que se nos presentaron aquí https://gitlab.com/nodo-mendoza/camara-respiracion-suelo/issues/3
### Fresadora
Los archivos digitales para la fresadora se hicieron tomando como guía el siguiente trabajo
http://cta.if.ufrgs.br/projects/estacao-meteorologica-modular/wiki/Placa_de_Controle,
donde se modificaron parámetros de acuerdo a las características específicas del proyecto como del tipo de fresadora utilizada.
Lo anterior descripto se puede observar aqui https://gitlab.com/nodo-mendoza/camara-respiracion-suelo/tree/master/hardware/electronics/placa_1/gCode/ngc
imágenes dd157eda80d9fbdee688e2e542551893808dbe02
### Montaje de la cámara
Utilizando 1 caço de PVC (20 cm de largo - 10 cm de diámetro) y dos tapas de PVC (10 cm diámetro) se diseço la cámara. Las tapas se colocaron una
sobre la otra, de esta forma la inferor sirve para cerrar el caço y lleva el sensor de CO2, y la superior contiene la placa con los componentes 
correspondientes (sensor de temperatura, teensy, display, bluetooth) dejando expuesto el display en la cara superior. 
Se puede observar aquí 6e3c59f4e5730dde76dc0a025e1911401b64a77b

